# react-template
This is a boilerplate for getting started in React.


It includes:

React, Redux, Sass, CSS Modules, Jest unit tests, usage of @ decorators from ES.next, imports with absolute paths, and basic Webpack, PostCSS, and Babel configs.

### Pre-reqs
Node >= 6

### Installation
Run `npm install` from the root directory, then start the app with `npm start`.

### Tests
Run `npm test` from the root directory.