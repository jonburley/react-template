var cssNext = require('postcss-cssnext')

module.exports = {
  plugins: [
    cssNext({
      features: {
        customProperties: {
          variables: {
            'preferred-font': '\'Open Sans\', sans-serif'
          }
        }
      }
    })
  ]
}
