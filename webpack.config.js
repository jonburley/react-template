var webpack = require('webpack')
var path = require('path')

module.exports = {
  entry: ['babel-polyfill', 'react-hot-loader/patch', './src/app.js'],
  output: {
    path: '/dist',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: [".jsx", ".json", ".js"],
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules')
    ]
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'client')
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: [{
          loader: "style-loader"
        },
        {
          loader: 'css-loader',
          options: {
            modules: true,
            localIdentName: '[local]___[hash:base64:5]',
            importLoaders: 2,
            sourceMap: true
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: true
          }
        }]
      }
    ]
  }
}