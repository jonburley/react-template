import { createStore as _createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

export default function createStore (history, data) {
  // get from node env later
  const dev = true
  const middleware = [thunk]

  let finalCreateStore
  if (dev) {
    const devMiddleware = [applyMiddleware(...middleware)]
    if (typeof window !== 'undefined' && window.devToolsExtension) {
      devMiddleware.push(window.devToolsExtension())
    }

    finalCreateStore = compose(...devMiddleware)(_createStore)
  } else {
    finalCreateStore = compose(applyMiddleware(...middleware))(_createStore)
  }

  const reducer = require('./root').default
  const store = finalCreateStore(reducer)

  if (dev && module.hot) {
    module.hot.accept('./root', () => {
      store.replaceReducer(require('./root'))
    })
  }

  return store
}
