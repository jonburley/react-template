import { combineReducers } from 'redux'

import demo from './modules/demo'

export default combineReducers({
  demo
})