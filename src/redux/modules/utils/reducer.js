export const setReducer = (state, { payload } = {}) => payload

export function createErrorReducerData (defaultError = '') {
  return (state, { payload = defaultError } = {}) => (
    Object.assign({}, state, {
      hasError: true,
      errorType: payload,
      loading: false
    })
  )
}

export function requestReducerData (state) {
  return Object.assign({}, state, { loading: true })
}

export function receiveReducerData (state, { payload } = {}) {
  const payloadWrapper = {}
  if (payload) {
    payloadWrapper.data = payload
  }

  const { hasError, errorType, ...stateRest } = state
  return Object.assign({}, stateRest, payloadWrapper, { loading: false })
}
