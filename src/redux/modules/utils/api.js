export function fetchAjax (url, { headers = {}, mock, ...options } = {}) {
  if (mock) {
    return Promise.resolve(mock)
  }

  const mergedHeaders = Object.assign({ 'Content-Type': 'application/json' }, headers)
  const mergedOptions = Object.assign({}, options, { headers: mergedHeaders })

  return fetch(url, mergedOptions)
    .then(response => response.ok ? response.json() : Promise.reject(response.status))
    .then(data => {
      return data
    })
    .catch(error => Promise.reject(error))
}
