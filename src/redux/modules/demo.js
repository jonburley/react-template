import { handleActions, createActions } from 'redux-actions'
import { fetchAjax } from './utils/api'
import { createErrorReducerData, requestReducerData, receiveReducerData } from './utils/reducer'

export const ERROR_CODES = {
  OTHER: 'OTHER'
}

const { errorDemo, requestDemo, receiveDemo } =
  createActions(
    'ERROR_DEMO',
    'REQUEST_DEMO',
    'RECEIVE_DEMO'
  )

export default handleActions({
  REQUEST_DEMO: requestReducerData,
  RECEIVE_DEMO: receiveReducerData,
  ERROR_DEMO: createErrorReducerData(ERROR_CODES.OTHER)
}, {})

export function fetchDemo() {
  return function (dispatch) {
    dispatch(requestDemo())
    const url = `/api/demo`
    const mock = { text: 'from demo redux store'}

    return fetchAjax(url, { mock })
      .then(data => dispatch(receiveDemo(data)))
      .catch(error => {
        dispatch(errorDemo())
        throw error
      })
  }
}
