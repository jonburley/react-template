import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchDemo } from 'redux/modules/demo'

import { DemoComponent } from 'components'

import styles from './DemoContainer.scss'

@connect(({ demo }) => ({ demo }))
export default class DemoContainer extends Component {
  static propTypes = {
    demo: PropTypes.object,
    dispatch: PropTypes.func
  }

  componentDidMount() {
    const { dispatch } = this.props

    dispatch(fetchDemo())
  }

  render () {
    const {
      demo: {
        data: demo = {}
      } = {}
    } = this.props

    return (
      <div className={styles.demoContainer}>
        <h2>DemoContainer</h2>
        <DemoComponent text='prop text' />
        <DemoComponent text={demo.text} />
        <DemoComponent />
      </div>
    )
  }
}
