import React from 'react'
import renderer from 'react-test-renderer'
import thunk from 'redux-thunk'
import toJson from 'enzyme-to-json'
import configureMockStore from 'redux-mock-store'
import { shallow } from 'enzyme'

import DemoContainer from '../DemoContainer'

const mockStore = configureMockStore([ thunk ])
const dispatch = jest.fn(() => {})

describe('DemoContainer', () => {
  test('renders all expected components without redux demo data', () => {
    const container = shallow(<DemoContainer.WrappedComponent dispatch={dispatch} />)
    expect(dispatch).toHaveBeenCalledTimes(1)
  })

  test('renders all expected components without redux demo data', () => {
    const container = shallow(<DemoContainer.WrappedComponent dispatch={dispatch} />)
    expect(toJson(container)).toMatchSnapshot()
  })

  test('renders all expected components with redux demo data', () => {
    const demo = {
      data: {
        text: 'from demo'
      }
    }
    const container = shallow(<DemoContainer.WrappedComponent { ...{dispatch, demo} } />)
    expect(toJson(container)).toMatchSnapshot()
  })

  test('renders all expected components with redux store', () => {
    const demo = {
      data: {
        text: 'from demo'
      }
    }
    const store = mockStore({ demo })
    const container = renderer.create(<DemoContainer { ...{ dispatch, store } } />)
    expect(container).toMatchSnapshot()
  })
})