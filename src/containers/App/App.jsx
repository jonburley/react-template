import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchDemo } from 'redux/modules/demo'

import { DemoContainer } from 'containers'

import styles from './App.scss'

export default class App extends Component {
  render () {
    return (
      <div className={styles.appContainer}>
        <h1>React Template App</h1>
        <DemoContainer />
      </div>
    )
  }
}
