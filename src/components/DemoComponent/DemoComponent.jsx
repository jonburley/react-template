import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './DemoComponent.scss'

export default class DemoComponent extends Component {
  static propTypes = {
    text: PropTypes.string
  }

  static defaultProps = {
    text: 'default text'
  }

  render () {
    const { text } = this.props
    return (
      <div className={styles.demoComponent}>
        <h3>DemoComponent - text: {text}</h3>
      </div>
    )
  }
}