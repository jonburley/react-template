import React from 'react'
import renderer from 'react-test-renderer'

import DemoComponent from '../DemoComponent'

describe('DemoComponent', () => {
  test('renders itself with defaults', () => {
    const component = renderer.create(<DemoComponent />)
    expect(component).toMatchSnapshot()
  })

  test('renders itself text prop', () => {
    const component = renderer.create(<DemoComponent text="test" />)
    expect(component).toMatchSnapshot()
  })
})